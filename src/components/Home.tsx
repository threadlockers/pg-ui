import * as React from "react";

// interface HomeProps {

// }

interface IHomeState {
    color: string;
}

export default class Home extends React.Component<any, IHomeState> {

    public render() {
        return (
            <div className="home">
                <h1>Welcome to PlantaGochi!</h1>
                <footer>Copyright &copy; 2018 ThreadLockers Studios</footer>
            </div>
        )
    }
}