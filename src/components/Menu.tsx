import * as React from "react";
// import { Link } from 'react-router-dom';

export default class Menu extends React.Component {
    public render() {
        return (
            <div className="menu">
                 <ul>
                     {
                         this.props.children
                     }
                </ul>
            </div>
        )
    }
}