import * as React from "react";
// import { userInfo } from "os";

interface ILoginState {
    userName: string;
    password: string;
    fieldsColor: string;
}

export default class Login extends React.Component<any, ILoginState> {
    public state = {
        password: '',
        userName: '',
        fieldsColor: ''
    };

    public handleKeyPress = (e: any) => {
        if (e.key === 'Enter') {
          this.dataChecker();
        }
      }

    public toogleFieldColor = () => {
        this.setState({fieldsColor: 'red'});
    }


    public handleLogin = () => {
        console.log(`Logging in with username:${this.state.userName} and password:${this.state.password}`);
        this.setState({fieldsColor: 'green'})
    };

    public handleInputChange = (event: any) => {
        const inputId: string = event.currentTarget.id;
        this.state[inputId] = event.currentTarget.value;
        this.setState(this.state);
    };

    public dataChecker = () => {
        if(this.state.userName !== '' && this.state.password!== ''){
            this.handleLogin();
        }   else {
            this.toogleFieldColor();
        }
            
    };

    public render() {
        return (
            <div className="login">
                <p> LOG IN </p>

                <div className='inputContainers'>
                    <form>
                        <label>
                            User:
                        </label>
                        <input 
                            id='userName'
                            value={this.state.userName}
                            onChange={this.handleInputChange}
                            style={{borderColor: this.state.fieldsColor}}
                            onKeyPress={this.handleKeyPress}

                        />

                        <label>
                            Password:
                        </label>
                        <input 
                            id='password' 
                            type="password" 
                            value={this.state.password}
                            onChange={this.handleInputChange}
                            style={{borderColor: this.state.fieldsColor}}
                            onKeyPress={this.handleKeyPress}
                        />
                    </form>
                </div>
                <div className='buttons'>
                    <button id='login' onClick={this.dataChecker}>Log in</button>
                    <a href='./Register'><button id='register'>Register</button></a>
                </div>
            </div>
        )
    }
}