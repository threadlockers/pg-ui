import * as React from "react";
// import { userInfo } from "os";

interface IRegisterState {
    email: string;
    userName: string;
    password: string;
    confpass: string;
    fieldsColor: string;
}

export default class Register extends React.Component<any, IRegisterState> {
    public state = {
        email: '',
        password: '',
        userName: '',
        confpass: '',
        fieldsColor: '',
        passFieldsColor: '',
    };

    public handleKeyPress = (e: any) => {
        if (e.key === 'Enter') {
          this.dataChecker();
        }
      }

    public toogleFieldColor = () => {
        this.setState({fieldsColor: 'red'});
    }


    public handleLogin = () => {
        console.log(`Registered with email:${this.state.email}, username: ${this.state.userName} and password:${this.state.password}`);
        this.setState({fieldsColor: 'green'})
    };

    public handleInputChange = (event: any) => {
        const inputId: string = event.currentTarget.id;
        this.state[inputId] = event.currentTarget.value;
        this.setState(this.state);
    };

    public dataChecker = () => {
        if(this.state.userName === '' || this.state.password === '' || this.state.email === '' || this.state.confpass === ''){
            this.toogleFieldColor();
        }   else {
            if(this.state.password !== this.state.confpass) {
                this.toogleFieldColor();
            } else {
                this.handleLogin();
            }
        }
            
    };

    public render() {
        return (
            <div className="register">
                <p> Register </p>

                <div className='inputContainers'>
                    <form>
                        <label>
                            Email
                        </label>
                        <input 
                            id='email'
                            value={this.state.email}
                            type="email"
                            onChange={this.handleInputChange}
                            style={{borderColor: this.state.fieldsColor}}
                            onKeyPress={this.handleKeyPress}
                        />

                        <label>
                            User
                        </label>


                        <input 
                            id='userName'
                            value={this.state.userName}
                            onChange={this.handleInputChange}
                            style={{borderColor: this.state.fieldsColor}}
                            onKeyPress={this.handleKeyPress}

                        />

                        <label>
                            Password
                        </label>
                        <input 
                            id='password' 
                            type="password" 
                            value={this.state.password}
                            onChange={this.handleInputChange}
                            style={{borderColor: this.state.fieldsColor}}
                            onKeyPress={this.handleKeyPress}
                        />

                        <label>
                            Confirm Password
                        </label>
                        <input 
                            id='confpass' 
                            type="password" 
                            value={this.state.confpass}
                            onChange={this.handleInputChange}
                            style={{borderColor: this.state.fieldsColor}}
                            onKeyPress={this.handleKeyPress}
                        />
                    </form>
                </div>
                <div className='buttons'>
                    <button id='register' onClick={this.dataChecker}>Register</button>
                </div>
            </div>
        )
    }
}