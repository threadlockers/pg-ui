import Dashboard from "./Dashboard";
import Database from "./Database";
import Logout from "./Logout";
import Plants from "./Plants";
import Users from "./Users";

import * as React from "react";

interface IPageState {
    adminPage: string;
}

export default class AdminPanel extends React.Component<any, IPageState> {
    
    public state = {
       adminPage: 'Dashboard',
    };


    public ActionArea = () => {
        console.log(this.state.adminPage);
        
        switch(this.state.adminPage) {
            case 'Dashboard':
                return <Dashboard/>;
            case 'Users':
                return <Users/>;
            case 'Database':
                return <Database/>;
            case 'Plants':
                return <Plants/>
            case 'Logout':
                return <Logout/>
            default:
                return <Dashboard/>;
        }

    }

    public handlePageChange = (event: any) => {
        this.state.adminPage = event.currentTarget.name;
        this.setState(this.state);
    };

    public render(){
        return (
            <div className="adminPanel">
                <div className="body-container">
                    <div className="adminList-container">
                        <button name="Dashboard" onClick={this.handlePageChange}>Dashboard</button>
                        <button name="Users" onClick={this.handlePageChange}>Users</button>
                        <button name="Database" onClick={this.handlePageChange}>Database</button>
                        <button name="Plants" onClick={this.handlePageChange}>Plants</button>
                        <button name="Logout" onClick={this.handlePageChange}>Logout</button>
                    </div>
                    <div className="actionArea-container">
                        {this.ActionArea()}
                    </div>
                </div>
            </div>
        )
    }
}