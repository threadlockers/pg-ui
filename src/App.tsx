import * as React from 'react';
import './App.css';
import AdminPanel from './components/AdminPanelComponents/AdminPanel';
import Home from './components/Home';
import Login from './components/Login';
import Register from './components/Register';

import {
  BrowserRouter as Router,
  Link,
  Route,
} from 'react-router-dom';
import Menu from './components/Menu';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <Router>
          <div>
            <Menu>
                <div className="leftCornerOfHeader">
                  <li><Link to="/" id="logo"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/2000px-React-icon.svg.png" alt=""/></Link></li>
                  <li><Link to="/">Home</Link></li>
                  <li><Link to="/adminpanel">Admin</Link></li>
              </div>
              <div className="rightCornerOfHeader">
                <li><Link to="/login">Login</Link></li>
                <li><Link to="/register">Register</Link></li>
              </div>
              
            </Menu>
            <Route 
              exact={true} path="/"
              component={Home}
            />
            <Route 
              path="/login" 
              component={Login}
            />
            <Route 
              path="/adminpanel"
              component={AdminPanel}
            />
            <Route
              path="/register"
              component={Register}
            />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
